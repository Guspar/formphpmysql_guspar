<!doctype html>
<html lang="en">
  <head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- Bootstrap CSS -->
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.1/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-+0n0xVW2eSR5OomGNYDnhzAbDsOXxcvSN1TPprVMTNDbiYZCxYbOOl7+AMvyTG2x" crossorigin="anonymous">

    <title>Form Penjadwalan Dosen</title>
  </head>
  <style>
  .container{
    background: cyan;
  }
  </style>  
  <body>
  <div class="container border border-primary ">
  <div class="row align-items-start">
        <form action="hasil.php" method="post">
        <center>
        <h2>Pendataan Penjadwalan Dosen</h2>
        </center>
        <div class="row">
            <label for="input_Nama_Dosen" class="col-sm-2 col-form-label">Nama Dosen</label>
            <div class="form-mb-20">
              <input type="text" class="form-control" id="input_Nama_Dosen" name="input_Nama_Dosen" >
            </div><br>
            <label for="input_NIP_Dosen" class="col-sm-2 col-form-label">NIP Dosen</label>
            <div class="form-mb-20">
              <input type="text" class="form-control" id="input_NIP_Dosen" name="input_NIP_Dosen" >
            </div><br>
            <label for="input_Fakultas" class="col-sm-2 col-form-label">Fakultas</label><br>
            <div class="form-mb-1">
            <select class="form-select" id="input_Fakultas" name="input_Fakultas" aria-label="Default select example">
                   <option selected></option>
                   <option value="FTK">FTK</option>
                   <option value="FIP">FIP</option>
                   <option value="FE">FE</option>
                   <option value="FOK">FOK</option>
                   <option value="FBS">FBS</option>
                   <option value="FMIPA">FMIPA</option>
                   <option value="FK">FK</option>
             </select>
             </div>
             <label for="input_Prodi" class="col-sm-2 col-form-label">Prodi</label>
             <div class="form-mb-1">
                <input type="text" class="form-control" id="input_Prodi" name="input_Prodi">
             </div><br>
             <label for="input_Kelas" class="col-sm-2 col-form-label">Nama Kelas</label>
             <div class="form-mb-1">
                <input type="text" class="form-control" id="input_Kelas" name="input_Kelas">
             </div><br>
             <label for="input_Jadwal" class="col-sm-2 col-form-label">Jadwal</label>
             <div class="form-mb-1">
                <input type="date" class="form-control" id="input_Jadwal" name="input_Jadwal">
             </div><br>
             <label for="input_Matakuliah" class="col-sm-2 col-form-label">Matakuliah</label>
             <div class="form-mb-1">
               <input type="text" class="form-control" id="input_Matakuliah" name="input_Matakuliah">
             </div>
             <div class="d-grid gap-2"><br>
             <button type="submit" class="btn btn-primary">Submit</button>
             </div><br>
             <div class="d-grid gap-4">
             <button type="reset" >Reset</button>
             </div>
        </form>
  </body>
  </html>