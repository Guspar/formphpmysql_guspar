<!doctype html>
<html lang="en">
  <head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- Bootstrap CSS -->
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.1/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-+0n0xVW2eSR5OomGNYDnhzAbDsOXxcvSN1TPprVMTNDbiYZCxYbOOl7+AMvyTG2x" crossorigin="anonymous">

    <title>Form Penjadwalan Dosen</title>
  </head>
  <body>
    <?php
        $namadosen = $_POST["input_Nama_Dosen"];
        $nipdosen = $_POST["input_NIP_Dosen"];
        $fakultas = $_POST["input_Fakultas"];
        $prodi = $_POST["input_Prodi"];
        $namakelas = $_POST["input_Kelas"];
        $jadwal = $_POST["input_Jadwal"];
        $matakuliah = $_POST["input_Matakuliah"];

        include "koneksi.php";

        $sql1 = "INSERT INTO dosen(nip_dosen,nama_dosen,prodi,fakultas) VALUES('".$nipdosen."','".$namadosen."','".$prodi."','".$fakultas."')";

        if(mysqli_query($conn,$sql1)){
              echo "Simpan data berhasil";
        }else{
              echo "Simpan data gagal!";
        }
        
        $sql2 = "INSERT INTO kelas(nama_kelas,prodi,fakultas) VALUES('".$namakelas."','".$prodi."','".$fakultas."')";
        
        if(mysqli_query($conn,$sql2)){
          echo "Simpan data berhasil";
        }else{
          echo "Simpan data gagal!";
        }
        
        $sql = "INSERT INTO jadwal_kelas(jadwal,mata_kuliah) VALUES('$jadwal','$matakuliah')";

        if(mysqli_query($conn,$sql)){
          echo "Simpan data berhasil";
        }else{
          echo "Simpan data gagal!";
        }
    ?>
<br>
 <div class="container border border-primary">
     <br>
  <center>
  <h2>Penjadwalan Dosen</h2>
  </center>
  <table class="table">
         <tr>
             <td class="col-sm-2">Nama Dosen</td>
             <td class="col-sm-4">: <?php echo "$namadosen"; ?></td>
         </tr>
         <tr>
             <td class="col-sm-2">NIP Dosen</td>
             <td class="col-sm-4">: <?php echo "$nipdosen"; ?></td>
         </tr>
         <tr>
             <td class="col-sm-2">Fakultas</td>
             <td class="col-sm-4">: <?php echo "$fakultas"; ?></td>
         </tr>
         <tr>
             <td class="col-sm-2">Prodi</td>
             <td class="col-sm-4">: <?php echo "$prodi"; ?></td>
         </tr>
         <tr>
             <td class="col-sm-2">Kelas</td>
             <td class="col-sm-4">: <?php echo "$namakelas"; ?></td>
         </tr>
         <tr>
             <td class="col-sm-2">Jadwal</td>
             <td class="col-sm-4">: <?php echo "$jadwal"; ?></td>
         </tr>
         <tr>
             <td class="col-sm-2">Matakuliah</td>
             <td class="col-sm-4">: <?php echo "$matakuliah"; ?></td>
         </tr>
     </table>
     <div class="mb-3">
         <button class="btn btn-primary" type="button">
             <a class="text-decoration-none text-light" href="input.php">Kembali</a>
         </button>
     </div>

     </br>
 </div>
</br>
  </body>
</html>